<?php

/* Template Name: Home */

get_header(); ?>

<div class="home-page">
    <?php if ( have_rows('slider_items') ) : ?>

        <div class="slider__wrapper">
        <?php while( have_rows('slider_items') ) : the_row(); ?>

        <div class="home-page__slider-image" style="background-image: url(<?php echo get_sub_field('image') ?>);">
            <div class="container">
                <div class="home-page__inner">
                    <div class="row home-page__inner-wrapper">
                        <div class="col-8">
                            
                            <?php if ( get_sub_field('title') ) : ?>
                                <h2 class="home-page__slider-title"><?php echo get_sub_field('title'); ?></h2>
                            <?php endif; ?>
                            
                            <?php if ( get_sub_field('text') ) : ?>
                                <p class="home-page__slider-text"><?php echo get_sub_field('text'); ?></p>
                            <?php endif; ?>
                            
                            <?php if ( get_sub_field('button_text') ) : ?>
                                <?php if ( get_sub_field('button_link') ) : ?>
                                    <a class="home-page__slider-button button" href="<?php echo get_sub_field('button_link'); ?>"><?php echo get_sub_field('button_text'); ?></a>
                                <?php endif; ?>
                            <?php endif; ?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php endwhile; ?>
        </div>
    <?php endif; ?>

    <div class="home-page__products">
        <div class="container">
            <div class="row">
                <?php if ( have_rows('product_items') ) : ?>
                
                    <?php while( have_rows('product_items') ) : the_row(); ?>
                
                        <div class="col-4 single-product">
                        <?php
                            if ( get_sub_field('image') ) :
                                $attachment_id = get_sub_field('image');
                                echo wp_get_attachment_image( $attachment_id, 'large', "", ["class" => "home-page__product-image"] );
                            ?>
                        <?php endif; ?>

                            <?php if ( get_sub_field('title') ) : ?>
                                <p class="home-page__product-title"><?php echo get_sub_field('title'); ?></p>
                            <?php endif; ?>
                            
                            <?php if ( get_sub_field('text') ) : ?>
                                <p class="home-page__product-text"><?php echo get_sub_field('text'); ?></p>
                            <?php endif; ?>
                            
                            <?php if ( get_sub_field('button_text') ) : ?>
                                <?php if ( get_sub_field('button_link') ) : ?>
                                    <a class="home-page__product-button button" href="<?php echo get_sub_field('button_link'); ?>"><?php echo get_sub_field('button_text'); ?></a>
                                <?php endif; ?>
                            <?php endif; ?>
                            
                        </div>
                
                    <?php endwhile; ?>
                
                <?php endif; ?>
                
            </div>
        </div>
    </div>

    <div class="home-page__about">
        <div class="container">
            <?php if ( have_rows('about_us') ) : ?>
            
                <?php while( have_rows('about_us') ) : the_row(); ?>
                     <div class="row home-page__about-inner">
                         
                    <?php if ( get_sub_field('title') ) : ?>
                        <h2 class="home-page__about-title"><?php echo get_sub_field('title'); ?></h2>
                    <?php endif; ?>
                    
                    <?php if ( get_sub_field('text') ) : ?>
                        <div class="home-page__about-text"><?php echo get_sub_field('text'); ?></div>
                    <?php endif; ?>
                    
                    <?php if ( get_sub_field('button_text') ) : ?>
                        <?php if ( get_sub_field('button_link') ) : ?>
                            <a class="home-page__about-button button" href="<?php echo get_sub_field('button_link'); ?>"><?php echo get_sub_field('button_text'); ?></a>
                        <?php endif; ?>
                    <?php endif; ?>
                    </div>
                <?php endwhile; ?>
            
            <?php endif; ?>
            
        </div>
    </div>

    <div class="home-page__partners">
        <div class="container">
            <h2 class="home-page__partner-title">PARTNEREK</h2>
            <div class="row home-page__partners-inner">
                <?php if ( have_rows('partner_items') ) : ?>
                
                    <?php while( have_rows('partner_items') ) : the_row(); ?>
                
                       <?php if ( get_sub_field('logo') ) : $image = get_sub_field('logo'); ?>
                       
                           <!-- Full size image -->
                           <img class="home-page__partner-image" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
                       
                       <?php endif; ?>
                       
                
                    <?php endwhile; ?>
                
                <?php endif; ?>
                
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>