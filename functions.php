<?php
/**
 * Peti functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Peti
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'peti_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function peti_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Peti, use a find and replace
		 * to change 'peti' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'peti', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'peti_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'peti_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function peti_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'peti_content_width', 640 );
}
add_action( 'after_setup_theme', 'peti_content_width', 0 );

/**
 * Enqueue scripts and styles.
 */
function peti_scripts() {
	wp_enqueue_style( 'peti-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'peti-style', 'rtl', 'replace' );

	wp_enqueue_script( 'peti-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	wp_enqueue_script('jquery');
	wp_enqueue_script( 'custom-script', get_template_directory_uri() . '/assets/js/custom.js', array(), filemtime( get_stylesheet_directory() . '/assets/js/custom.js' ));

}
add_action( 'wp_enqueue_scripts', 'peti_scripts' );

/**
 * Enqueue scripts.
 */
function peti_slider_scripts(){
    wp_enqueue_script(
            'slick',
            get_stylesheet_directory_uri() . '/assets/js/slick.min.js',
            ['jquery']
        );
}
add_action( 'wp_enqueue_scripts', 'peti_slider_scripts' );
    
function peti_slider_styles(){
    wp_enqueue_style(
		'slick',
		get_stylesheet_directory_uri() . '/assets/css/slick.css',
	);
}
add_action( 'wp_enqueue_scripts', 'peti_slider_styles' );


/**
 * Register Option ACF Pages.
 */
function peti_register_options_pages() {

	if( function_exists('acf_add_options_page') ) {
		acf_add_options_page();

		acf_add_options_sub_page('Footer');
	}
}
add_action('init', 'peti_register_options_pages');


/*
* Register navigation menus
*/
function peti_register_menus() {
	register_nav_menus( array(
		'primary-menu' => __('Primary menu'),
	) );
 }
add_action( 'after_setup_theme', 'peti_register_menus' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

