<header class="navigation">
	<div class="nav-color first"></div>
	<div class="container">
		<div class="navigation__wrapper">

		<?php if ( function_exists( 'the_custom_logo' ) ) :
			the_custom_logo();
		endif; ?>

		<?php wp_nav_menu([
				'theme_location' => 'primary-menu',
				'container'=> false,
				'menu_id'=>'primary-menu',
				'menu_class' => 'navigation__desktop-menu'
				]); ?>

			<div class="block-navigation__hamburger-box">
				<div class="block-navigation__hamburger-inner">
				</div>
			</div>
		</div>

	</div>
	<div class="nav-color second">
	</div>
</header>
