<?php wp_footer(); ?>

<footer>
	<div class="footer">
		<div class="container">

			<div class="row">
				
				<div class="footer__contact">
					<?php if ( get_field('name', 'option') ) : ?>
						<div class="footer__contact__icons">
							<img src="<?php bloginfo('template_directory'); ?>/assets/images/name.svg" class=" footer__contact__icon footer__contact__name"><p class="footer__name"><?php echo get_field('name', 'option'); ?></p>
						</div>
					<?php endif; ?>

					<?php if ( get_field('address', 'option') ) : ?>
						<div class="footer__contact__icons">
						<img src="<?php bloginfo('template_directory'); ?>/assets/images/location.svg" class="footer__contact__icon footer__contact__location"><p class="footer__address"><?php echo get_field('address', 'option'); ?></p>
						</div>
					<?php endif; ?>
					
					<?php if ( get_field('phone', 'option') ) : ?>
						<div class="footer__contact__icons">
						<img src="<?php bloginfo('template_directory'); ?>/assets/images/phone.svg" class="footer__contact__icon footer__contact__phone"><a href="tel:" class="footer__phone"><?php echo get_field('phone', 'option'); ?></a>
						</div>
					<?php endif; ?>

					<?php if ( get_field('e-mail', 'option') ) : ?>
						<div class="footer__contact__icons">
						<img src="<?php bloginfo('template_directory'); ?>/assets/images/mail.svg" class="footer__contact__icon footer__contact__mail"><a href="mailto:" class="footer__e-mail"><?php echo get_field('e-mail', 'option'); ?></a>
						</div>
					<?php endif; ?>
				</div>

				<div class="footer__logo-inner">
					<?php if ( get_field('logo', 'option') ) : $image = get_field('logo', 'option'); ?>
						<!-- Full size image -->
						<img class="footer__logo" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
					<?php endif; ?>
				</div>
			</div>
	</div>
</div>
<div class="footer__bottom">
	<span>Copyright © <?php echo(date('Y')); ?></span>
</div>

</footer>


</body>
</html>
